﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IdentityCore.Models;
using IdentityCore.Data;
using System.Net.Mail;

namespace IdentityCore.Controllers
{
    public class HomeController : Controller
    {
        public  ApplicationDbContext applicationDbContext { get; set; }

        public HomeController(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }

        public IActionResult Index()
        {
            

            return View(applicationDbContext.Products.ToList());
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        [HttpPost]
        public IActionResult Contact(Email email)
        {
            

            
            string subject = email.Subject;
            string body = email.Body;

            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add("ownershopify@gmail.com");
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.From = new MailAddress("ownershopify@gmail.com");
            mailMessage.IsBodyHtml = false;          

            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.Port = 587;
            smtp.UseDefaultCredentials = true;
            smtp.EnableSsl = true;
            smtp.Credentials = new System.Net.NetworkCredential("ownershopify@gmail.com", "9t4x3a6xCKBZStQ");
            smtp.Send(mailMessage);

            ViewBag.message = "The mail has been sent successfully!";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

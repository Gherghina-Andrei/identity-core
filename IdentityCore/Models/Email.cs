﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityCore.Models
{
    public class Email
    {
        public string Sender { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }
}
